from flask import render_template, flash, redirect, url_for
from app import app, db, models
from .models import Book

@app.route('/')
def index():
    books = models.Book.query.all()
    completed = models.Book.query.filter_by(read=1).count()
    return render_template('allBooks.html', title='All books', books=books, completed=completed)

@app.route('/unread')
def unread():
    unreadBooks = models.Book.query.filter_by(read=False).all()
    return render_template('unreadBooks.html', title='Unread Books', books=unreadBooks)

@app.route('/read')
def read():
    readBooks = models.Book.query.filter_by(read=True).all()
    return render_template('readBooks.html', title='Read Books', books=readBooks)

@app.route('/markAsRead/<id>')
def markAsRead(id):
    book = models.Book.query.get(id)
    book.read = 1
    db.session.commit()
    return redirect('/')

@app.route('/scifan')
def scifan():
    books = models.Book.query.all()
    return render_template('scifan.html', title='Science Fiction and Fantasy', books=books)
