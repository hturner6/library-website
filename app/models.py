from app import db

class Book(db.Model):
    rank = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(500), unique=True)
    author = db.Column(db.String(500))
    read = db.Column(db.Boolean)
