# Library Website
## created by Harrison Turner

Used in order to track progress of the top 100 Science Fiction and Fantasy novels of all time.

Highlights books as read or unread and can be changed with the click of a button.

The website can be sorted, in order to make the viewing of data easier. Also tracks how many books out of a hundred have been read
