# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## Unreleased
### Changed
- Nothing yet

### [0.0.1] - 08-02-2019
- Added Changelog (nice)
- Added Top100SciFi pages
- Added Navigation Bar
- Added Colourscheme